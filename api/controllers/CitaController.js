/**
 * CitaController
 *
 * @description :: Server-side logic for managing citas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    
    create: function(req,res) {
        
        Cita.create(req.body).exec((error, data) => {
            
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },

    update: function(req,res) {
        Cita.update({id: req.params.id}, req.body).exec((error, cita)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: cita
                })
            }
        })
    },

    find: function(req,res) {
        Cita.find().populate('id_client').exec((error, cita)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: cita
                })
            }
        })
    },

    findOne: function(req,res) {
        Cita.findOne({id: req.params.id}).exec((error, cita)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: cita
                })
            }
        })
    }

};

