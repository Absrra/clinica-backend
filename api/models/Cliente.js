/**
 * Cliente.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema: true,
  autoPK: true,
  attributes: {
    cedula: {
      type: 'integer',
      unique: true,
      required: true
    },
    nombre: {
      type: 'string'
    },
    apellido: {
      type: 'string'
    },
    direccion: {
      type: 'string'
    },
    email: {
      type: 'email',
      unique: true
    },
    phone: {
      type: 'integer'
    }
  }
};

