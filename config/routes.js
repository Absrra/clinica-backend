/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/':                                        'HomeController',
  /** Rutas cliente */
  'POST /client':                              'ClienteController.create',
  'PUT  /client/:id':                          'ClienteController.update',
  'GET  /client':                              'ClienteController.find',
  'GET  /client/:id':                          'ClienteController.findOne',

  /** Rutas citas */
  'POST /cita':                                'CitaController.create',
  'PUT  /cita/:id':                            'CitaController.update',
  'GET  /cita':                                'CitaController.find',
  'GET  /cita/:id':                            'CitaController.findOne',

  /** Rutas examen */
  'POST /examen':                              'ExamenController.create',
  'PUT  /examen/:id':                          'ExamenController.update',
  'GET  /examen':                              'ExamenController.find',
  'GET  /examen/:id':                          'ExamenController.findOne',

  /** Rutas Medico */
  'POST /medico':                              'MedicoController.create',
  'PUT  /medico/:id':                          'MedicoController.update',
  'GET  /medico':                              'MedicoController.find',
  'GET  /medico/:id':                          'MedicoController.findOne',

  /** Rutas Análisis */
  'POST /analisis':                            'AnalisisController.create',
  'PUT  /analisis/:id':                        'AnalisisController.update',
  'GET  /analisis':                            'AnalisisController.find',
  'GET  /analisis/:id':                        'AnalisisController.findOne',

  /** Rutas Historia */
  'POST /historia':                            'HistoriaController.create',
  'PUT  /historia/:id':                        'HistoriaController.update',
  'GET  /historia':                            'HistoriaController.find',
  'GET  /historia/:id':                        'HistoriaController.findOne',

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
